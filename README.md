# CarCar

This is an app that could theoretically be used to manage sales/inventory/ and car data of a dealership.

Team:

<<<<<<< HEAD

* Gary L - Automobile Service
* Serge L - Automobile Sales
>>>>>>> main

## How to Run this App

*Please ensure you have Docker, Git, and Node.js 18.2 or above.

- Fork this repo.

- Clone the newly forked repo onto your local machine by using:
  git clone <<repository.url>>

-Use the following commands to build and run the docker containers:
    docker volume create beta-data
    docker-compose build
    docker-compose up

-Check docker to ensure all containers are up and spinning

-visit http://localhost:3000/ to see the application.

## Diagram
 https://imgur.com/a/gCRpP8G


~Technicians APIs

 List all technicians - GET - http://localhost:8080/api/technicians/
 Create a technician - POST - http://localhost:8080/api/technicians/
 Delete a technician - DELETE - http://localhost:8080/api/technicians/<int:pk>/


LIST ALL TECHNICIANS: Does what it says, no strings attached. Will list all current technicians.

CREATE A NEW TECHNICIAN - To add someone new, copy and paste the example below as a JSON POST request.

{
    "first_name": "Gary",
    "last_name": "Le",
    "employee_id": "1"
}

If you pasted that code block into insomnia/postman, and you do a list request, you should see it listed as it is below. The extra "id" is included in the encoder to help the app keep track of each person. The
employee_id is no a unique feature, so it can be reused if desired due to the system always assigning a unique id.
{
	"technicians": [
		{
        "first_name": "Gary",
        "last_name": "Le",
        "employee_id": "1"
        "id": 1
		},
    ]
}


DELETE TECHNICIAN - Again, does what it says. send a DELETE method request to the api listed above with replacing <int:pk> with any technicians' "id"


~Appointments API
List all Appointments - GET request - http://localhost:8080/api/appointments/
Create a new appointment - POST request - http://localhost:8080/api/appointments/
Delete an appointment - DELETE request - http://localhost:8080/api/appointments/:id/
Cancel an appointment - PUT request - http://localhost:8080/api/appointments/:id/cancel/
Finish an appointment - PUT request -	http://localhost:8080/api/appointments/:id/finish/

LIST SERVICE APPOINTMENT - This will return a list of all current appointments, Status will be defaulty be set to "Created" until changed through the button features on the appointments list page.

{
    "appointments": [
        {
            "vin": "12345678901234567",
            "status": "Created",
            "date_time": "2023-12-08T08:35:00+00:00",
            "customer": "Greg",
            "reason": "Tires",
            "technician": {
                "first_name": "Gary",
                "last_name": "le",
                "employee_id": "123456",
                "id": 1
            },
            "id": 5
        },


CREATE SERVICE APPOINTMENT - Copying and pasting the code block below will create a new service appointment through insomnia. You can then view the appointment by sending the list request. the VIN field should be 17 characters or less, and needs to be unique, so if you create more than one appointment, be sure to change the vin. Also, this information relies on technician data, if there is no technician with the corresponding "id" of 1, then this will fail. Be sure to create technicians before doing this step.

{
    "vin": "12345678901234567",
    "date_time": "2023-12-31 11:00",
    "customer": "Gary",
    "reason": "Transmission",
    "technician": 1
}

DELETE SERVICE APPOINTMENT - To delete any appointment, you can list them, grab the "id" field, and plug it into the delete URL found above. You should be met with a similar message as when listing, the key difference would be that the "id" is now null. There is no "deleted" status, so it will remain the same to what it was before deletion.

{
    "vin": "1A2B3C4D5E6F6G7H8",
    "status": "Created",
    "date_time": "2023-12-19T17:48:00+00:00",
    "customer": "Cory",
    "reason": "Exhaust",
    "technician": {
        "first_name": "Jane",
        "last_name": "Sundymanns",
        "employee_id": "314",
        "id": 16
    },
    "id": null
}


***The apis that end with cancel and finish are set up as PUT requests to update the "status" field of an appointment

to use them just select one of them from above and copy/paste the following:
{
    "status": "Cancelled"
}

"Cancelled" can be replaced with "Finished" if that is what you prefer.
--------------------------------------------------------------------------------------------------------------------------------

### Sales API

Models:
Salesperson Model:
Fields: first_name, last_name, employee_id

Customer Model:
Fields: first_name, last_name, address, phone_number

Sale Model:
Fields: automobile, salesperson, customer, price

AutomobileVO Model:
Fields: vin, sold


 - Put Sales API documentation here
Salespeople API documentation


Action	                        Method	                URL
List salespeople    	        GET	        http://localhost:8090/api/salespeople/
Create a salesperson	        POST	    http://localhost:8090/api/salespeople/
Delete a specific salesperson	DELETE	    http://localhost:8090/api/salespeople/:id/

List of All Salesperson: created a list of salespeople containing the following fields first-name, last-name and employee_id

Create A New Saleperson - To add someone new, copy and paste the example below as a JSON POST request.
the front end view REACT.js will contain the following fields: first-name, last-name and employee_id and a submit button



{
  "first_name": "Jane",
  "last_name": "Doe",
  "employee_id": "54321"
}
--------------------------------------------------------------------------------------------------------------------------------


Action	                        Method	                URL
List customers	                GET	        http://localhost:8090/api/customers/
Create a customer	            POST	    http://localhost:8090/api/customers/
Delete a specific customer	    DELETE	    http://localhost:8090/api/customers/:id/


List of All Customers: created a list of customers containing the following fields first-name, last-name, address and phone-number

Create A New Customer - To add someone new, copy and paste the example below as a JSON POST request.
the front end view REACT.js will contain the following fields: first-name, last-name and address, phone-number and a submit button


{
  "first_name": "Allan",
  "last_name": "Peo",
  "address": "433 Main St, Cityville",
  "phone_number": "310-614-1234"
}

--------------------------------------------------------------------------------------------------------------------------------



Action	                        Method	                URL
List sales	                    GET	        http://localhost:8090/api/sales/
Create a sale	                POST	    http://localhost:8090/api/sales/
Delete a sale	                DELETE	    http://localhost:8090/api/sales/:id


List of All Sold Vehicles: created a list containing employee_id, employee_name, customers_name, vin_number, and price.

Salesperson Sales History: this if the same list containing the sales information but with a dropdown with the name of the salesperson so that the user is able to filter by the name of the salesperson

Create A New Record of Sale - To add someone new, copy and paste the example below as a JSON POST request.
the front end view REACT.js will contain the following fields: dropdown for vin_number of available vehicles, full name of Salesperson, full name of customerthe price input fiels and a create button/ uppond creating a record of sale the vehicle will be removed from avalible vehicles and marked as sold 




{
    "automobile": 22412412345323,
    "salesperson": 3,
    "customer": 1,
    "price": 250000.00
}
--------------------------------------------------------------------------------------------------------------------------------
