from django.contrib import admin

# Register your models here.
from .models import  Sale, AutomobileVO


admin.site.register(Sale)
admin.site.register(AutomobileVO)
