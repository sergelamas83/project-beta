# sales/sales_rest/urls.py

from django.urls import path
from sales_rest.api_views import (
    api_salespeople,
    api_salesperson,
    api_customers,
    api_customer,
    api_sale,
    api_sales,
   # api_sale_history
    

)

urlpatterns = [
    # Salespeople URLs
    path('salespeople/', api_salespeople, name='api_salespeople'),
    path('salespeople/<int:id>/', api_salesperson, name='api_salesperson'),

    # Customers URLs
    path('customers/', api_customers, name='api_customers'),
    path('customers/<int:id>/', api_customer, name='api_customer'),

    # Sales URLs
    path('sales/', api_sales, name='api_sales'),
    path('sales/<int:id>/', api_sale, name='api_sale'),
    #path('sales/history/', api_sale_history, name='api_sale_history')   
]
