import React, { useEffect, useState } from 'react';

function AutomobilesList() {
  const [automobiles, setAutomobiles] = useState([]);

  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');

      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        console.error('Error fetching automobile data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {automobiles.map(automobile => (
          <tr key={automobile.id}>
            <td>{automobile.vin}</td>
            <td>{automobile.color}</td>
            <td>{automobile.year}</td>
            <td>{automobile.model.name}</td>
            <td>{automobile.model.manufacturer.name}</td>
            <td>
              {automobile.sold ? (
                <span style={{ color: 'red' }}>Sold</span>
              ) : (
                <span style={{ color: 'green' }}>Available</span>
              )}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default AutomobilesList;


