import { useEffect, useState } from 'react';


function HistoryList() {
  const [appointments, setAppointments] = useState([])
  const [search, setSearch] = useState('')
  const getData = async () => {
  const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  useEffect(()=>{
    getData()
  }, [])



  return (

    <div>
        <nav className="navbar navbar-light bg-light">
            <form className="form-inline">
                <input onChange={(e) => setSearch(e.target.value)}
                 className="form-control mr-sm-2" type="search" placeholder="Search by VIN #" aria-label="Search"/>
                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search VIN</button>
            </form>
        </nav>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Reason</th>
          <th>Status</th>
          <th>Technician</th>
        </tr>
      </thead>
      <tbody>
        {appointments.filter((item) => {
            return search.toLowerCase() === '' ? item : item.vin.toLowerCase().includes(search)
        }).map(apps => {
          return (
            <tr key={apps.id}>
              <td>{ apps.vin }</td>
              <td>{ apps.customer }</td>
              <td>{ apps.date_time }</td>
              <td>{ apps.date_time }</td>
              <td>{ apps.reason }</td>
              <td>{ apps.status }</td>
              <td>{ apps.technician.first_name + " " + apps.technician.last_name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </div>
  );
}

export default HistoryList;