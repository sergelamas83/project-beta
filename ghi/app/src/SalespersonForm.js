import React, { useEffect, useState } from 'react';

function SalespersonForm() {
  const [first_name, setFirstName] = useState('');
  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const [last_name, setLastName] = useState('');
  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const [employee_id, setEmployeeId] = useState('');
  const handleEmployeeIdChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  const [salespeople, setSalespersons] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      first_name: first_name,
      last_name: last_name,
      employee_id: employee_id,
    };

    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const salespersonUrl = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {
      setFirstName('');
      setLastName('');
      setEmployeeId('');
    }
  };

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/salespeople/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespersons(data.salespeople);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new salesperson!</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleFirstNameChange}
                  placeholder="First Name"
                  required
                  type="text"
                  id="first_name"
                  name="first_name"
                  className="form-control"
                  value={first_name}
                />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleLastNameChange}
                  placeholder="Last Name"
                  required
                  type="text"
                  id="last_name"
                  name="last_name"
                  className="form-control"
                  value={last_name}
                />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleEmployeeIdChange}
                  placeholder="Employee ID"
                  required
                  type="text"
                  id="employee_id"
                  name="employee_id"
                  className="form-control"
                  value={employee_id}
                />
                <label htmlFor="employee_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalespersonForm;
