import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function TechniciansList() {
  const [technicians, setTechnicians] = useState([])

  const getData = async () => {
  const response = await fetch('http://localhost:8080/api/technicians/');

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians)
    }
  }

  const deleteTech = async (event) => {
  const techID = (event.target.id);
  const url = `http://localhost:8080/api/technicians/${techID}/`
  const response = await fetch(url, {method: 'DELETE'})
        .then((response) => {
            if(!response.ok){
                throw new Error('You messed up');
            }
            getData()
        })
    }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map(techs => {
          return (
            <tr key={techs.id}>
              <td>{ techs.first_name }</td>
              <td>{ techs.last_name}</td>
              <td>{ techs.employee_id }</td>
              <td><NavLink to="#" className="btn btn-primary" onClick={deleteTech} id={techs.id}>Delete Tech</NavLink></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default TechniciansList;