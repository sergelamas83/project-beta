import React, { useEffect, useState } from 'react';


function AutomobileForm(){

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const [year, setYear] = useState('');
    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const [vin, setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const [model_id, setModelID] = useState('');
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModelID(value);
    }

    const [models, setModels] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.color = color;
        data.year = year
        data.vin = vin;
        data.model_id = model_id;


        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        console.log(fetchConfig);

        const autosUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(autosUrl, fetchConfig);
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModelID('');
          }
      }

    const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);

        }
      }

      useEffect(() => {
        fetchData();
      }, []);

      return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Automobile!</h1>
              <form onSubmit={handleSubmit} id="create-model-form">
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} placeholder="Color" required type="text" id="color" name="color" className="form-control" value={color} />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleYearChange} placeholder="Year" required type="number" id="year" name="year" className="form-control" value={year} />
                  <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleVinChange} placeholder="VIN" required type="text" id="vin" name="vin" className="form-control" value={vin} />
                  <label htmlFor="vin">VIN</label>
                </div>
                <div className="mb-3">
                  <select value={model_id} onChange={handleModelChange} required id="model_id" name="model_id" className="form-select" >
                    <option value="">Choose a model</option>
                        {models.map(model => {
                            return (
                              <option key={model.id} value={model.id}>
                              {model.name}
                              </option>
                            );
                            })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}

export default AutomobileForm;
