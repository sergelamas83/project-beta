import React, { useEffect, useState } from 'react';


function ModelForm(){

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [picture_url, setPictureUrl] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const [manufacturer_id, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const [manufacturers, setManufacturers] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.picture_url = picture_url;
        data.manufacturer_id = manufacturer_id;


        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const modelUrl = 'http://localhost:8100/api/models/';
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setPictureUrl('');
            setManufacturer('');
          }
      }

    const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers);

        }
      }

      useEffect(() => {
        fetchData();
      }, []);

return (
    <div className="container">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Model!</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" value={name} />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureChange} placeholder="Picture URL" required type="text" id="picture_url" name="picture_url" className="form-control" value={picture_url} />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select value={manufacturer_id} onChange={handleManufacturerChange} required id="manufacturer" name="manufacturer" className="form-select" >
                <option value="">Choose a manufacturer</option>
                    {manufacturers.map(manufacturer => {
                        return (
                          <option key={manufacturer.id} value={manufacturer.id}>
                          {manufacturer.name}
                          </option>
                        );
                        })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  </div>
)
}

export default ModelForm;
