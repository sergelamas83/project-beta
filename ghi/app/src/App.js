import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './Manufacturers';
import ModelsList from './Models';
import AutomobilesList from './Automobiles';
import ManufacturerForm from './ManufacturerForm';
import ModelForm from './ModelForm';
import AutomobileForm from './AutomobileForm';
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import RecordSaleForm from './RecordSaleForm';
import AllSales from './AllSales';
import SalespersonHistory from './SalespersonHistory';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './Technicians';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './Appointments';
import HistoryList from './History';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/models" element={<ModelsList />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route path="/models/create" element={<ModelForm />} />
          <Route path="/automobiles/create" element={<AutomobileForm />} />
          <Route path="/salesperson" element={<SalespersonList />} />
          <Route path="/salesperson/create" element={<SalespersonForm />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/create" element={<CustomerForm />} />
          <Route path="/sales/create" element={<RecordSaleForm />} />
          <Route path="/sales" element={<AllSales />} />
          <Route path="/sales/history" element={<SalespersonHistory/>} />
          <Route path="/technicians/create" element={<TechnicianForm />} />
          <Route path="/technicians/" element={<TechniciansList />} />
          <Route path="/appointments/create" element={<AppointmentForm />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/history" element={<HistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
