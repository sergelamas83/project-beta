from django.shortcuts import render
import json
from .models import AutomobileVO, Technician, Appointment
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "import_href"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "status",
        "date_time",
        "customer",
        "reason",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def del_technician(request, id):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(pk=id)
            technician.delete()
            return JsonResponse({"message": "Tech deleted successfully!"})
        except Technician.DoesNotExist:
            return JsonResponse({"error": "Tech does not exist"}, status=404)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except:
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response

        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_appointment(request, id):
    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(pk=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            appointment = Appointment.objects.get(pk=id)
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_list_history(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    if request.method == "PUT":
        Appointment.objects.filter(id=id).update(status="Finished")
        appointments = Appointment.objects.get(id=id)
        return JsonResponse(
            appointments,
            AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    if request.method == "PUT":
        Appointment.objects.filter(id=id).update(status="Canceled")
        appointments = Appointment.objects.get(id=id)
        return JsonResponse(
            appointments,
            AppointmentEncoder,
            safe=False,
        )