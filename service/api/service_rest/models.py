from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=50)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True)


class Appointment(models.Model):

    date_time = models.DateTimeField()
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)

    technician = models.ForeignKey(
        Technician,
        related_name="tech",
        on_delete=models.CASCADE,
    )
