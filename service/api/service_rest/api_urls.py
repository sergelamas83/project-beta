from django.urls import path
from service_rest.views import api_appointment, api_list_appointments, api_list_history, api_list_technicians, cancel_appointment, del_technician, finish_appointment

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>/", del_technician, name="del_technician"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/", api_appointment, name="api_appointment"),
    path("appointments/history/", api_list_history, name="api_list_history"),
    path("appointments/<int:id>/finish/", finish_appointment, name='finish_appointment'),
    path("appointments/<int:id>/cancel/", cancel_appointment, name='cancel_appointment'),
]
